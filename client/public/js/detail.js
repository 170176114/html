var hostname = "http://localhost:3000/"
$(document).ready(function () {
	$(".menu-icon").on("click", function () {
		$("nav ul").toggleClass("showing");
	});

	$("#logout").click(function () {
		$.ajax({
			url: hostname + "logout",
			type: "GET",
			dataType: "jsonp",
			success: function (data) {
				window.location.href = '/';
			}
		})
	})



	$('#create').click(function () {
		window.location.href = '/create';
	});


	$("#edit").click(function () {
		var product_id = $("#product_id").val()
		var Productdescription = $("#Productdescription").val()
		var Price = $("#Price").val()
		var Location = $("#Location").val()
		var Telephone = $("#Telephone").val()
		
		if (Productdescription == "" || Price == "" || Location == "" || Telephone.length == ""){
			alert( "Product Description or Price or Location or Telephone is not null")
		}else if (Telephone.length === 8){
			var edit_data = {
				item_id: product_id,
				Productdescription: Productdescription,
				Price: Price,
				Location: Location,
				Telephone: Telephone
			}
	
			fetch("http://localhost:3000/item", {
				method: 'PUT', // or 'PUT'
				body: JSON.stringify(edit_data), // data can be `string` or {object}!
				headers: new Headers({
					'Content-Type': 'application/json'
				})
			}).then(res => res.json())
				.catch(error => console.error('Error:', error))
				.then(response => history.go(0));
			location.reload()
		}else {
			alert("TeletePhone number length must 8")
		}

		

	})


	$("#delete_item").click(function () {
		var product_id = $("#product_id").val()
		var data = { item_id: product_id };
		fetch(hostname + "item", {
			method: 'delete', // or 'PUT'
			body: JSON.stringify(data), // data can be `string` or {object}!
			headers: new Headers({
				'Content-Type': 'application/json'
			})
		}).then(res => res.json())
			.catch(error => console.error('Error:', error))
			.then(response => window.location.href = "/main");
	})

	$("#add_comment").click(function (){
		var comment = $("#comment").val()
		var product_id = $("#product_id").val()
		var username = $("#CheckPermission").val()
		//console.log(username)

		$.ajax({
			url: hostname + "comment",
			data: { 
				ProductID: product_id, 
				Comment: comment,
				Username: username
			},
			type: "POST",
			dataType: "application/jsonp",
			success: function (data) {
				location.reload();
			}
		});
		location.reload();
	})






})






// Scrolling Effect

$(window).on("scroll", function () {
	if ($(window).scrollTop()) {
		$('nav').addClass('black');
	}

	else {
		$('nav').removeClass('black');
	}
})

setTimeout(function() {
$.ajax({
	url: hostname + "username",
	type: "GET",
	dataType: "jsonp",
	success: function (data) {
		if (data.username.username != "") {
			$("#username").text(data.username.username)
			$("#CheckPermission").val(data.username.username)

		} else {
			window.location.href = '/';
		}

	}
})
},100)

var product_id = $("#product_id").val()


$.ajax({
	url: hostname + "item/" + product_id,
	type: "GET",
	dataType: "jsonp",
	success: function (data) {
		//console.log(data.item)
		$("#product-description").append(
			'<h1>' +
			data.item.ProductName +
			'</h1>' +
			'<p>' +
			data.item.Productdescription +
			'</p>'
		)
		$(".left-column").append(
			'<img class="active" src="http://localhost:3000/image/' + data.item.ProductImage + '" alt="">'
		)
		$(".detail").append(
			'<p>' +
			"Price: $" + data.item.Price +
			'</p>' + '<br>' +
			'<p>' +
			"Location: " + data.item.Location +
			'</p>' + '<br>' +
			'<p>' +
			"Telephone: " + data.item.Telephone +
			'</p>' + '<br>' +
			'<p>' +
			"Created by: " + data.item.Username +
			'</p>'

		)
		var check = $("#CheckPermission").val()
		if (check === data.item.Username) {
			$('#change').show()
		} else {
			$('#change').hide()
		}
		//console.log(data)

	}
})

$.ajax({
	url: hostname + "comment/" + product_id,
	type: "GET",
	dataType: "jsonp",
	success: function (data) {
		var check = $("#CheckPermission").val()
		for (i = 0; i < data.item.length; i++) {
		console.log(data.item[i])
		$("#all_comment").append(
			// data[i].Comment + '<br>'
			'<div class="panel panel-default">' +
            '<div class="panel-heading">' +
			'<strong>' + data.item[i].Username + '</strong>' +
			( check == data.item[i].Username ?
				'<div>' +
				'<div class="row">' +
				'<div class="col-sm">' +
				'</div>' +
				'<div class="col-sm">' +
				'</div>' +
				'<div class="col-sm">' +
				'<form onsubmit="setTimeout(function () { window.location.reload(); }, 10)" method="post" action="http://localhost:3000/comment/{{id}}?_method=DELETE">' +
				'<input type="hidden" name="_method" value="' + data.item[i]._id + '">' + 
				'<button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#ModalCenter' + i + '">Edit</button>' +
				'<button type="submit" onClick="location.reload()" class="btn btn-outline-danger" id="Delete_Comment' + i + '"' + '>' + '<img src="../static/css/images/cross.png"></img>'  + '</button>' +
				'</form>' +
				'</div>'  +
			  '</div>' +
			 '</div>': "" ) +
            '</div>' +
            '<div class="panel-body">' +
            data.item[i].Comment +
            '</div>'+
			'</div>' +




			'<div class="modal fade" id="ModalCenter'+ i +'" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">' +
			'<div class="modal-dialog modal-dialog-centered" role="document">' +
			'<div class="modal-content">' +
			'<div class="modal-header">' +
			'<h5 class="modal-title" id="exampleModalCenterTitle">Modify Your Choosing Commnet</h5>' +
			'<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
			'<span aria-hidden="true">&times;</span>' +
			'</button>' +
			'</div>' +
			'<form onsubmit="setTimeout(function () { window.location.reload(); }, 10)" method="POST" action="http://localhost:3000/comment/{{id}}?_method=PUT">' +
			'<div class="modal-body">' +
			'<input type="hidden" name="selected_id" value=' + data.item[i]._id + '>' +
			'<input type="text" class="form-control" name="change_comment" required>'+
			'</div>' +
			'<div class="modal-footer">' +
			'<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
			'<button type="submit" class="btn btn-primary">Save changes</button>' +
			'</div>' +
			'</form>'+
			'</div>' +
			'</div>'


		)

		}
		
	}
})
